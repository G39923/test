/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl_javafx01_.pkg39923;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author quasar
 */
public class BMR extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Calcul du BMR");
        VBox root = new VBox(15);
        MenuBar mnBar = new MenuBar();
        Menu mnFile = new Menu("File");
        MenuItem mnexit = new MenuItem("Exit");
        mnexit.setOnAction(event -> System.exit(0));
        mnFile.getItems().add(mnexit);
        mnBar.getMenus().add(mnFile);
        root.getChildren().add(mnBar);
        HBox hb = new HBox();
        GridPane griDonnées = new GridPane();
        GridPane griRésultat = new GridPane();
        griDonnées.setPadding(new Insets(10));
        griDonnées.setHgap(2);
        griDonnées.setVgap(5);
        griRésultat.setPadding(new Insets(10));
        griRésultat.setHgap(2);
        griRésultat.setVgap(5);
        Label labDon = new Label("Données");
        labDon.setUnderline(true);
        griDonnées.add(labDon, 0, 0);
        Label labTail = new Label("Taille (cm)");
        griDonnées.add(labTail, 0, 1);
        Label labPoid = new Label("Poids (kg)");
        griDonnées.add(labPoid, 0, 2);
        Label labAge = new Label("Age (années)");
        griDonnées.add(labAge, 0, 3);
        Label labSex = new Label("Sexe");
        griDonnées.add(labSex, 0, 4);
        Label labStyl = new Label("Style de vie");
        griDonnées.add(labStyl, 0, 5);
        TextField texTail = new TextField();//"Taille en cm");
        griDonnées.add(texTail, 1, 1);
        TextField texPoid = new TextField();//"Poids en kilo");
        griDonnées.add(texPoid, 1, 2);
        TextField texAge = new TextField();//"Age en Années");
        griDonnées.add(texAge, 1, 3);
        RadioButton rb1 = new RadioButton("Femme");
        RadioButton rb2 = new RadioButton("Homme");
        rb1.setSelected(true);
        ToggleGroup togGroup = new ToggleGroup();
        rb1.setToggleGroup(togGroup);
        rb2.setToggleGroup(togGroup);
        HBox r1 = new HBox();
        r1.getChildren().addAll(rb1, rb2);
        griDonnées.add(r1, 1, 4);
        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList(
                "Sedentaire", "Peu actif", "Actif", "Fort actif", "Extrêment actif"));
        cb.setValue("Sedentaire");
        griDonnées.add(cb, 1, 5);
        Label labRes = new Label("Resultat");
        labRes.setUnderline(true);
        griRésultat.add(labRes, 0, 0);
        Label labBMR = new Label("BMR");
        griRésultat.add(labBMR, 0, 1);
        Label labCal = new Label("Calories");
        griRésultat.add(labCal, 0, 2);
        TextField texBmr = new TextField();//"BMR");
        texBmr.setEditable(false);
        griRésultat.add(texBmr, 1, 1);
        TextField texCal = new TextField();//"Calories");
        texCal.setEditable(false);
        griRésultat.add(texCal, 1, 2);
        hb.getChildren().addAll(griDonnées, griRésultat);
        root.getChildren().add(hb);
        Button calculBtn = new Button("Calcul du BMR");
        calculBtn.setMaxWidth(Double.MAX_VALUE);
        calculBtn.setOnAction(event -> {
            int bmr = 0;
            boolean error = false;
            int poids = 0;
            int taille=0;
            int age=0;
            try {
                poids = Integer.parseInt(texPoid.getText());
                taille = Integer.parseInt(texTail.getText());
                age = Integer.parseInt(texAge.getText());
            } catch (NumberFormatException nfe) {
                error = true;
                texBmr.setText("Failed");
                texCal.setText("Failed");
            }
            if (!error) {
                if(poids<0 || poids>600){
                    createAlertBox("Valeur du poids erronée","Vous devez encoder...");
                }else if(taille<0 || taille>300){
                    createAlertBox("Valeur de la taille erronée","Vous devez encoder...");
                }else if(age<0 || age>130){
                    createAlertBox("Valeur de l'age erronée","Vous devez encoder...");
                }
                if (rb1.isSelected()) {
                    bmr += 9.6 *poids;
                    bmr += 1.8 *taille;
                    bmr -= 4.7 * age;
                    bmr += 655;
                } else {
                    bmr += 13.7 * poids;
                    bmr += 5 * taille;
                    bmr -= 6.8 * age;
                    bmr += 66;
                }
                texBmr.setText(String.valueOf(bmr));
                switch (cb.getValue().toString()) {
                    case ("Sedentaire"):
                        bmr *= 1.2;
                        break;
                    case ("Peu actif"):
                        bmr *= 1.375;
                        break;
                    case ("Actif"):
                        bmr *= 1.55;
                        break;
                    case ("Fort actif"):
                        bmr *= 1.725;
                        break;
                    case ("Extrêment actif"):
                        bmr *= 1.9;
                        break;
                }
                texCal.setText(String.valueOf(bmr));
            }
        });
        root.getChildren().add(calculBtn);
        Button clearBtn = new Button("Clear");
        clearBtn.setOnAction(event -> {
            texPoid.setText("");
            texTail.setText("");
            texAge.setText("");
            rb1.setSelected(true);
            cb.setValue("Sedentaire");
            texBmr.setText("");
            texCal.setText("");
        });
        clearBtn.setMaxWidth(Double.MAX_VALUE);
        root.getChildren().add(clearBtn);
        Scene scene = new Scene(root, 550, 320);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public void createAlertBox(String strH,String strC){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erreur d'encodage");
        alert.setHeaderText(strH);
        alert.setContentText(strC);
        alert.showAndWait();
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
